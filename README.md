## MyMSACS+2

ASP.NET Core app for planning a Northwest degree.

44-663 C#.NET Fall 2017

## Getting Started

Recommended: Visual Studio 2017 Community

Run Build / Build Solution and make sure the project compiles.

Go to Tools / NuGet Package Manager / Package Manager Console. 

If the migrations are not already available (or if the schema changes) then add a new script (where initial is the name you give this set of changes)

```
add-migration initial
```
If the SQL Server LocalDB 'MyMSACS+2' is not already available, then execute the migration scripts with:

```
update-database
```

## Contributors

-Mani Vikash Vanaparthi
-Sainath Gulla
-Sashidhar Siddamsetty
-Siva Reddy Mekapothula


