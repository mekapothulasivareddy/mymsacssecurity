﻿using MyMSACSSecurity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xunit;

namespace MyMSACSSecurity.Tests.Models
{
    public class StudentsTest
    {
        [Fact]
        public void CreatesStudentWithGivenInformation()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                GivenName = "Test",
                FamilyName = "Test"  
                
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
            Assert.Equal("Test Test", model.FullName);
        }

        [Fact]
        public void Student_GivenName_Shouldnothave_allspaces()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                GivenName = "D",
                FamilyName = "Case"               
                
            };
            var context = new ValidationContext(model, null, null);
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);

            var failure = Assert.Single(validationResults,
                  x => x.ErrorMessage == "Given Name must be longer than 1 character");
            Assert.Single(failure.MemberNames, x => x == "GivenName");

        }

        //ManiVikash Vanaparthi

        [Fact]
        public void Should_Not_Create_Student_Without_FamilyName()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                GivenName = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);  // should not be successful
        }

        //Sashidhar Siddamsetty

        [Fact]
        public void Should_Not_Create_Student_Without_GivenName()
        {
            // Arrange
            var model = new Student
            {
                StudentId = 123,
                FamilyName = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
        }

        //SivaReddy Mekapothula
        [Fact]
        public void Given_Name_must_be_short()
        {
            // Arrange
            var model = new Student()
            {
                StudentId = 1234,
                GivenName = "Test5Test0Test5Test0Test5Test0Test5Test0Test5Test0Test",
                FamilyName = "Test"
            };
            var context = new ValidationContext(model, null, null);
            var result = new List<ValidationResult>();
           

            // Act
            var success = Validator.TryValidateObject(model, context, result, true);

            // Assert
            Assert.False(success);
            var failure = Assert.Single(result,
                x => x.ErrorMessage == "Given Name cannot be longer than 50 characters");
            Assert.Single(failure.MemberNames, x => x == "GivenName");
        }
    }
}
