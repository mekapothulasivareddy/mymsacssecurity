﻿using MyMSACSSecurity.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Xunit;

namespace MyMSACSSecurity.Tests.Models
{
    public class StudentDegreesTest
    {
        [Fact]
        public void CreatesDegreeWithGivenInformation()
        {
            // Arrange
            var model = new StudentDegree
            {
                StudentDegreeId = 123,
                DegreeName = "Test",
                DegreeAbbrev = "Test"
            };
            var validationResults = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.True(success);
        }

        //ManiVkash Vanaparthi

        [Fact]
        public void Should_Not_Create_Degree_Without_DegreeAbbrev()
        {
            // Arrange
            var model = new StudentDegree
            {
                StudentDegreeId = 123,
                DegreeName = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success);  // should not be successful
        }


        //Sashidhar Siddamsetty

        [Fact]
        public void Should_Not_Create_Degree_Without_DegreeName()
        {
            // Arrange
            var model = new StudentDegree
            {
                StudentDegreeId = 123,
                DegreeAbbrev = "Test"
            };
            var validationResults = new List<ValidationResult>();

            //Act
            var success = Validator.TryValidateObject(model, new ValidationContext(model), validationResults, true);

            // Assert 
            Assert.False(success); // should not be successful
        }



        //SivaReddy Mekapothula

        [Fact]
        public void Degree_abbrev_must_be_short()
        {
            // Arrange
            var model = new StudentDegree()
            {
                StudentDegreeId = 1234,
                DegreeAbbrev = "123456789012345678901234567890123456789012345678901234567890",
                DegreeName = "Test"
            };
            var context = new ValidationContext(model, null, null);
            var result = new List<ValidationResult>();

            // Act
            var success = Validator.TryValidateObject(model, context, result, true);

            // Assert
            Assert.False(success);
            var failure = Assert.Single(result,
                x => x.ErrorMessage == "Degree Abbreviation cannot be longer than 10 characters");
            Assert.Single(failure.MemberNames, x => x == "DegreeAbbrev");
        }


    }
}
