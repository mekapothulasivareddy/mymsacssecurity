﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class Student
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentId { get; set; }

        [Required]
        [Display(Name = "Given Name")]
        [StringLength(50, ErrorMessage = "Given Name cannot be longer than 50 characters")]
        public string GivenName { get; set; }

        [Required]
        [Display(Name = "Family Name")]
        [StringLength(50, ErrorMessage = "Last Name cannot be longer than 50 characters")]
        public string FamilyName { get; set; }

        [Display(Name = "Full Name")]
        public string FullName
        {
            get
            {
                return GivenName + " " + FamilyName;
            }
        }

        public ICollection<StudentDegreePlan> StudentDegreePlans { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentId = " + StudentId +
              "GivenName = " + GivenName +
              ", FamilyName = " + FamilyName +
              ", FullName = " + FullName +
              "";
        }
    }
}
