﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegreeStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreeStatusId { get; internal set; }

        [Required]
        [Display(Name = "Degree Status")]
        [StringLength(20, ErrorMessage = "Degree Status cannot be longer than 20 characters")]
        public string DegreeStatus { get; internal set; }

        // Add navigation property for each related entity

        // each degree status includes zero, one, or many student degree plans
        //public ICollection<StudentDegreePlan> StudentDegreePlans { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreeStatusId = " + StudentDegreeStatusId +
              ", DegreeStatus = " + DegreeStatus +
              "";
        }
    }
}
