﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegreeRequirementStatus
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreeRequirementStatusId { get; internal set; }

        [Required]
        [Display(Name = "Requirement Status")]
        [StringLength(20, ErrorMessage = "Requirement Status cannot be longer than 20 characters")]
        public string RequirementStatus { get; internal set; }

        // Add navigation property for each related entity

        // each requirement status includes zero, one, or many plan term requirements (things we hope to get done that term)
        //public ICollection<PlanTermRequirement> PlanTermRequirements { get; set; }


        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreeRequirementStatusId = " + StudentDegreeRequirementStatusId +
              ", RequirementStatus = " + RequirementStatus +
              "";
        }
    }
}
