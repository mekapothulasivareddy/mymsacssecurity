﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegreePlan
    {
        public StudentDegreePlan()
        {
            StudentDegreeStatusId = 1; //default to 1
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreePlanId { get; set; }

        public int StudentId { get; set; }

        public int StudentDegreeId { get; set; }

        [Required]
        [Display(Name = "Plan Number")]
        [StringLength(10, ErrorMessage = "Plan Number cannot be longer than 10 characters")]
        public int PlanNumber { get; set; }

        [Required]
        [Display(Name = "Plan Abbreviation")]
        [StringLength(20, ErrorMessage = "Plan Abbreviation cannot be longer than 20 characters")]
        public string PlanAbbrev { get; set; }

        [Required]
        [Display(Name = "Plan Name")]
        [StringLength(50, ErrorMessage = "Plan Name cannot be longer than 10 characters")]
        public string PlanName { get; set; }

        public int StudentDegreeStatusId { get; set; }

        [Display(Name = "Includes Internship?")]
        public Boolean IncludesInternship { get; set; }

        // Add navigation property for each related entity

        // each plan points to exactly one student
        public Student Student { get; set; }

        // each plan points to exactly one degree
        public StudentDegree StudentDegree { get; set; }

        // each plan points to exactly one degree status
        public StudentDegreeStatus StudentDegreeStatus { get; set; }

        // each plan has many terms... 
        public ICollection<StudentDegreePlanTerm> StudentDegreePlanTerms { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreePlanId = " + StudentDegreePlanId +
              "StudentId = " + StudentId +
              ", StudentDegreeId = " + StudentDegreeId +
              ", PlanNumber = " + PlanNumber +
              ", PlanAbbrev = " + PlanAbbrev +
              ", PlanName = " + PlanName +
              ", StudentDegreeStatusId = " + StudentDegreeStatusId +
              ", IncludesInternship = " + IncludesInternship;
        }
    }
}
