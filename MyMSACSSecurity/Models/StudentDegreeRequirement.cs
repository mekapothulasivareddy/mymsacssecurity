﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegreeRequirement
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreeRequirementId { get; set; }

        public int StudentDegreeId { get; set; }

        // required not needed - ints will always have a value
        [Display(Name = "Requirement Number (used for sorting)")]
        public int RequirementNumber { get; set; }

        [Required]
        [Display(Name = "Requirement Abbreviation")]
        [StringLength(10, ErrorMessage = "Abbreviation cannot be longer than 10 characters.")]
        public string RequirementAbbrev { get; set; }

        [Required]
        [Display(Name = "Requirement Name")]
        [StringLength(60, ErrorMessage = "Name cannot be longer than 60 characters.")]
        public string RequirementName { get; set; }

        // Add navigation property for each related entity

        // each degree requirement points to exactly one degree
        public StudentDegree StudentDegree { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreeRequirementId = " + StudentDegreeRequirementId +
              "StudentDegreeId = " + StudentDegreeId +
              ", RequirementNumber = " + RequirementNumber +
              ", RequirementAbbrev = " + RequirementAbbrev +
              ", RequirementName = " + RequirementName;
        }
    }
}
