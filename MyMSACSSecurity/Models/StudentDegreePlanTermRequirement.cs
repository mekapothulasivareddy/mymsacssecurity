﻿                                                                                                                                                                                                                                                                                                            using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegreePlanTermRequirement
    {
        public StudentDegreePlanTermRequirement()
        {
            StudentDegreeRequirementStatusId = 1; // default to 1
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreePlanTermRequirementId { get; internal set; }

        public int StudentDegreePlanTermId { get; internal set; }

        [Display(Name = "Term Number (used for sorting)")]
        public int TermNumber { get; internal set; }

        [Display(Name = "Requirement Number (used for sorting)")]
        public int RequirementNumber { get; internal set; }

        public int StudentDegreeRequirementStatusId { get; internal set; }

        // Add navigation property for each related entity

        // each plan term requirement points to exactly one plan term
        public StudentDegreePlanTerm StudentDegreePlanTerm { get; set; }

        // each plan term requirement points to exactly one requirement status - have we earned credit in this term or not?
        public StudentDegreeRequirementStatus StudentDegreeRequirementStatus { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreePlanTermRequirementId = " + StudentDegreePlanTermRequirementId +
              "StudentDegreePlanTermId = " + StudentDegreePlanTermId +
              ", TermNumber = " + TermNumber +
              ", RequirementNumber = " + RequirementNumber +
              ", RequirementStatusId = " + StudentDegreeRequirementStatusId;
        }
    }
}
