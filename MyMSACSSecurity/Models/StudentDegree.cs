﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Models
{
    public class StudentDegree
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int StudentDegreeId { get; set; }

        [Required]
        [Display(Name = "Degree Name")]
        [StringLength(110, ErrorMessage = "Degree Name cannot be longer than 50 characters")]
        public string DegreeName { get; set; }

        [Required]
        [Display(Name = "Degree Abbreviation")]
        [StringLength(10, ErrorMessage = "Degree Abbreviation cannot be longer than 10 characters")]
        public string DegreeAbbrev { get; set; }

        public ICollection<StudentDegreeRequirement> StudentDegreeRequirements { get; set; }

        public override string ToString()
        {
            return base.ToString() + ": " +
              "StudentDegreeId = " + StudentDegreeId +
              ", DegreeAbbrev = " + DegreeAbbrev +
              ", DegreeName = " + DegreeName +
              "";
        }
    }
}
