﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyMSACSSecurity.Models;

namespace MyMSACSSecurity.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<StudentDegreeStatus> StudentDegreeStatuses { get; set; }
        public DbSet<StudentDegreeRequirementStatus> StudentDegreeRequirementStatuses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<StudentDegree> StudentDegrees { get; set; }
      
        public DbSet<StudentDegreeRequirement> StudentDegreeRequirements { get; set; }
        public DbSet<StudentDegreePlan> StudentDegreePlans { get; set; }
        public DbSet<StudentDegreePlanTerm> StudentDegreePlanTerms { get; set; }
        public DbSet<StudentDegreePlanTermRequirement> StudentDegreePlanTermRequirements { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity<StudentDegreeStatus>().ToTable("StudentDegreeStatus");
            builder.Entity<StudentDegreeRequirementStatus>().ToTable("StudentDegreeRequirementStatus");
            builder.Entity<Student>().ToTable("Student");
            builder.Entity<StudentDegree>().ToTable("StudentDegree");
     

            builder.Entity<StudentDegreeRequirement>().ToTable("StudentDegreeRequirement");
            builder.Entity<StudentDegreePlan>().ToTable("StudentDegreePlan");
            builder.Entity<StudentDegreePlanTerm>().ToTable("StudentDegreePlanTerm");
            builder.Entity<StudentDegreePlanTermRequirement>().ToTable("StudentDegreePlanTermRequirement");
        }
    }
}
