﻿using Microsoft.Extensions.Logging;
using MyMSACSSecurity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyMSACSSecurity.Data
{
    public class DbInitializer
    {
        private static readonly ILogger LOG = ProgramLogger.CreateLogger();

        public DbInitializer()
        {
            LOG.LogInformation("Logger available.");
        }
        public static void Initialize(ApplicationDbContext context)
        {
            // context.Database.EnsureCreated();

            if (!context.StudentDegreeStatuses.Any())
            {
                var studentDegreeStatuses = new StudentDegreeStatus[]
                {
                    new StudentDegreeStatus{StudentDegreeStatusId=1,DegreeStatus="Planned" },
                    new StudentDegreeStatus{StudentDegreeStatusId=2,DegreeStatus="In-Progress" },
                    new StudentDegreeStatus{StudentDegreeStatusId=3,DegreeStatus="Completed" }
                };

                foreach (StudentDegreeStatus item in studentDegreeStatuses)
                    try
                    {
                        context.StudentDegreeStatuses.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}.", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }
            }

            if (!context.StudentDegreeRequirementStatuses.Any())
            {

                var studentDegreeRequirementStatuses = new StudentDegreeRequirementStatus[]
                {
                    new StudentDegreeRequirementStatus{StudentDegreeRequirementStatusId=1,RequirementStatus="Planned" },
                    new StudentDegreeRequirementStatus{StudentDegreeRequirementStatusId=2,RequirementStatus="In-Progress" },
                    new StudentDegreeRequirementStatus{StudentDegreeRequirementStatusId=3,RequirementStatus="Completed" }
                };

                foreach (StudentDegreeRequirementStatus item in studentDegreeRequirementStatuses)
                {
                    context.StudentDegreeRequirementStatuses.Add(item);
                    context.SaveChanges();
                    LOG.LogDebug("Added {}", item);
                }

            }

            // Look for any students.
            if (!context.Students.Any())
            {
                var students = new Student[]
                {
                new Student{StudentId=1531,GivenName="Peanut",FamilyName="McNubbin"},
                new Student{StudentId=1647,GivenName="Waffles",FamilyName="Frapenstein"},
                new Student{StudentId=1840,GivenName="Butters",FamilyName="Bunnybill"},
                new Student{StudentId=1537,GivenName="Bella",FamilyName="Barkley"},
                new Student{StudentId=1675,GivenName="Max",FamilyName="Headroom"},
                new Student{StudentId=1745,GivenName="Charlie",FamilyName="Goodchap"},
                new Student{StudentId=1206,GivenName="Buddy",FamilyName="CoolJ"},
                new Student{StudentId=1966,GivenName="Patch",FamilyName="Shakespaw"},
                new Student{StudentId=1683,GivenName="Pickles",FamilyName="Pooch"},
                new Student{StudentId=1333,GivenName="Honey",FamilyName="Dawg"},
                new Student{StudentId=1606,GivenName="Abbie",FamilyName="Fangle"},
                new Student{StudentId=1954,GivenName="Tess",FamilyName="Ruff"},
                new Student{StudentId=1528,GivenName="Denise",FamilyName="Case"}
                };

                foreach (Student item in students)
                {
                    context.Students.Add(item);
                    context.SaveChanges();
                    LOG.LogDebug("Added {}", item);
                }
            }


            if (!context.StudentDegrees.Any())
            {

                var studentDegrees = new StudentDegree[]
                {
                    new StudentDegree{StudentDegreeId=1526,DegreeAbbrev="MSACS+2",DegreeName="Masters of Applied Computer Science (with 2 rereqs)"},
                    new StudentDegree{StudentDegreeId=1050,DegreeAbbrev="MSACS+NF",DegreeName="Masters of Applied Computer Science (with NF rereq)"},
                    new StudentDegree{StudentDegreeId=1547,DegreeAbbrev="MSACS+DB",DegreeName="Masters of Applied Computer Science (with DB rereq)"},
                    new StudentDegree{StudentDegreeId=1824,DegreeAbbrev="MSACS",DegreeName="Masters of Applied Computer Science"},
                    new StudentDegree{StudentDegreeId=1829,DegreeAbbrev="MSIS",DegreeName="Masters of Information Systems"},
                    new StudentDegree{StudentDegreeId=1850,DegreeAbbrev="MSIT",DegreeName="Masters of Information Technology"}
                };
                foreach (StudentDegree item in studentDegrees)
                {
                    try
                    {
                        context.StudentDegrees.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }
                }
            }

            if (!context.StudentDegreeRequirements.Any())
            {
                var studentDegreeRequirements = new StudentDegreeRequirement[] {
              new StudentDegreeRequirement {StudentDegreeRequirementId = 1046, StudentDegreeId = 1526, RequirementNumber = 1, RequirementAbbrev = "356", RequirementName = "Network Fundamentals" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1523, StudentDegreeId = 1526, RequirementNumber = 2, RequirementAbbrev = "460", RequirementName = "Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1963, StudentDegreeId = 1526, RequirementNumber = 3, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1521, StudentDegreeId = 1526, RequirementNumber = 4, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1383, StudentDegreeId = 1526, RequirementNumber = 5, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1409, StudentDegreeId = 1526, RequirementNumber = 6, RequirementAbbrev = "555", RequirementName = "Network Security" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1738, StudentDegreeId = 1526, RequirementNumber = 7, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1890, StudentDegreeId = 1526, RequirementNumber = 8, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1380, StudentDegreeId = 1526, RequirementNumber = 9, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1191, StudentDegreeId = 1526, RequirementNumber = 10, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1921, StudentDegreeId = 1526, RequirementNumber = 11, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1755, StudentDegreeId = 1526, RequirementNumber = 12, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1742, StudentDegreeId = 1526, RequirementNumber = 13, RequirementAbbrev = "elective", RequirementName = "Elective" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1990, StudentDegreeId = 1526, RequirementNumber = 14, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1741, StudentDegreeId = 1050, RequirementNumber = 1, RequirementAbbrev = "356", RequirementName = "Network Fundamentals" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1640, StudentDegreeId = 1050, RequirementNumber = 2, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1102, StudentDegreeId = 1050, RequirementNumber = 3, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1651, StudentDegreeId = 1050, RequirementNumber = 4, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1813, StudentDegreeId = 1050, RequirementNumber = 5, RequirementAbbrev = "555", RequirementName = "Network Security" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1501, StudentDegreeId = 1050, RequirementNumber = 6, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1312, StudentDegreeId = 1050, RequirementNumber = 7, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1361, StudentDegreeId = 1050, RequirementNumber = 8, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1111, StudentDegreeId = 1050, RequirementNumber = 9, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1946, StudentDegreeId = 1050, RequirementNumber = 10, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1294, StudentDegreeId = 1050, RequirementNumber = 11, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1058, StudentDegreeId = 1050, RequirementNumber = 12, RequirementAbbrev = "elective", RequirementName = "Elective" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1373, StudentDegreeId = 1050, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1016, StudentDegreeId = 1547, RequirementNumber = 1, RequirementAbbrev = "460", RequirementName = "Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1017, StudentDegreeId = 1547, RequirementNumber = 2, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1831, StudentDegreeId = 1547, RequirementNumber = 3, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1389, StudentDegreeId = 1547, RequirementNumber = 4, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1486, StudentDegreeId = 1547, RequirementNumber = 5, RequirementAbbrev = "555", RequirementName = "Network Security" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1949, StudentDegreeId = 1547, RequirementNumber = 6, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1714, StudentDegreeId = 1547, RequirementNumber = 7, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1158, StudentDegreeId = 1547, RequirementNumber = 8, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1195, StudentDegreeId = 1547, RequirementNumber = 9, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1643, StudentDegreeId = 1547, RequirementNumber = 10, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1497, StudentDegreeId = 1547, RequirementNumber = 11, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1884, StudentDegreeId = 1547, RequirementNumber = 12, RequirementAbbrev = "elective", RequirementName = "Elective" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1004, StudentDegreeId = 1547, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1797, StudentDegreeId = 1824, RequirementNumber = 1, RequirementAbbrev = "542", RequirementName = "Object-Oriented Programming" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1498, StudentDegreeId = 1824, RequirementNumber = 2, RequirementAbbrev = "563", RequirementName = "Developing Web Applications and Services" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1166, StudentDegreeId = 1824, RequirementNumber = 3, RequirementAbbrev = "560", RequirementName = "Advanced Topics in Database Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1093, StudentDegreeId = 1824, RequirementNumber = 4, RequirementAbbrev = "555", RequirementName = "Network Security" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1885, StudentDegreeId = 1824, RequirementNumber = 5, RequirementAbbrev = "618", RequirementName = "Project Management in Business and Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1023, StudentDegreeId = 1824, RequirementNumber = 6, RequirementAbbrev = "623", RequirementName = "Information Technology Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1134, StudentDegreeId = 1824, RequirementNumber = 7, RequirementAbbrev = "mobile", RequirementName = "Mobile Computing" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1006, StudentDegreeId = 1824, RequirementNumber = 8, RequirementAbbrev = "664", RequirementName = "Human Computer Interaction" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1622, StudentDegreeId = 1824, RequirementNumber = 9, RequirementAbbrev = "691", RequirementName = "CS Graduate Directed Project I" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1079, StudentDegreeId = 1824, RequirementNumber = 10, RequirementAbbrev = "692", RequirementName = "CS Graduate Directed Project II" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1918, StudentDegreeId = 1824, RequirementNumber = 11, RequirementAbbrev = "elective", RequirementName = "Elective" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1798, StudentDegreeId = 1824, RequirementNumber = 12, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1875, StudentDegreeId = 1829, RequirementNumber = 1, RequirementAbbrev = "ITM", RequirementName = "Information Technology Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1824, StudentDegreeId = 1829, RequirementNumber = 2, RequirementAbbrev = "ISAD", RequirementName = "Information Systems Analysis and Design" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1853, StudentDegreeId = 1829, RequirementNumber = 3, RequirementAbbrev = "OOS", RequirementName = "Developing Object-Oriented Systems with Java" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1847, StudentDegreeId = 1829, RequirementNumber = 4, RequirementAbbrev = "DDI", RequirementName = "Database Design and Implementation" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1231, StudentDegreeId = 1829, RequirementNumber = 5, RequirementAbbrev = "ENI", RequirementName = "Enterprise Networking and Internetworking" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 2000, StudentDegreeId = 1829, RequirementNumber = 6, RequirementAbbrev = "SDE", RequirementName = "User Centered System Design and Evaluation" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1677, StudentDegreeId = 1829, RequirementNumber = 7, RequirementAbbrev = "INFOSEC", RequirementName = "Cybersecurity and Information Systems Security Management" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1417, StudentDegreeId = 1829, RequirementNumber = 8, RequirementAbbrev = "PISE", RequirementName = "Professionalism in the IS Environment" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1927, StudentDegreeId = 1829, RequirementNumber = 9, RequirementAbbrev = "PMIT", RequirementName = "Project Management for Business and Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1575, StudentDegreeId = 1829, RequirementNumber = 10, RequirementAbbrev = "FMIT", RequirementName = "Financial Modeling and Decision Making for IT" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1909, StudentDegreeId = 1829, RequirementNumber = 11, RequirementAbbrev = "BIA", RequirementName = "Business Intelligence and Analytics" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1692, StudentDegreeId = 1829, RequirementNumber = 12, RequirementAbbrev = "CAP", RequirementName = "IS Capstone Project" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1075, StudentDegreeId = 1829, RequirementNumber = 13, RequirementAbbrev = "comps", RequirementName = "Comprehensive Exam" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1530, StudentDegreeId = 1850, RequirementNumber = 1, RequirementAbbrev = "44-515 ", RequirementName = "Effective Assessment " },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1257, StudentDegreeId = 1850, RequirementNumber = 2, RequirementAbbrev = "44-582", RequirementName = "Technology Curriculum & Integration" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1429, StudentDegreeId = 1850, RequirementNumber = 3, RequirementAbbrev = "44-585", RequirementName = "Instructional Technology and the Learning Process" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1900, StudentDegreeId = 1850, RequirementNumber = 4, RequirementAbbrev = "44-614", RequirementName = "Introduction to Online Teaching/Learning" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1453, StudentDegreeId = 1850, RequirementNumber = 5, RequirementAbbrev = "44-626", RequirementName = "Multimedia Systems" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1883, StudentDegreeId = 1850, RequirementNumber = 6, RequirementAbbrev = "44-635", RequirementName = "Instructional Systems Design" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1397, StudentDegreeId = 1850, RequirementNumber = 7, RequirementAbbrev = "44-645", RequirementName = "Computers and Networks" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1524, StudentDegreeId = 1850, RequirementNumber = 8, RequirementAbbrev = "44-650   ", RequirementName = "Building Virtual Learning Environment" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1318, StudentDegreeId = 1850, RequirementNumber = 9, RequirementAbbrev = "44-656  ", RequirementName = "Current Issues in Instructional Technology" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1381, StudentDegreeId = 1850, RequirementNumber = 10, RequirementAbbrev = "44-696", RequirementName = "Graduate Directed Project" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1697, StudentDegreeId = 1850, RequirementNumber = 11, RequirementAbbrev = "Elective", RequirementName = "Elective" },
              new StudentDegreeRequirement { StudentDegreeRequirementId = 1319, StudentDegreeId = 1850, RequirementNumber = 12, RequirementAbbrev = "comps", RequirementName = "IS Capstone Project" }
            };
                foreach (StudentDegreeRequirement item in studentDegreeRequirements)
                {
                    try
                    {
                        context.StudentDegreeRequirements.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }

                }
            }


            if (!context.StudentDegreePlans.Any())
            {
                var studentDegreePlans = new StudentDegreePlan[]
                {
                    new StudentDegreePlan{StudentDegreePlanId=1672, StudentId=1531,StudentDegreeId=1526,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1617, StudentId=1647,StudentDegreeId=1050,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1147, StudentId=1840,StudentDegreeId=1547,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1334, StudentId=1537,StudentDegreeId=1824,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1234, StudentId=1675,StudentDegreeId=1829,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1076, StudentId=1745,StudentDegreeId=1850,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1661, StudentId=1206,StudentDegreeId=1526,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1133, StudentId=1966,StudentDegreeId=1050,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1519, StudentId=1683,StudentDegreeId=1547,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1277, StudentId=1333,StudentDegreeId=1824,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1065, StudentId=1333,StudentDegreeId=1829,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1105, StudentId=1606,StudentDegreeId=1829,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan{StudentDegreePlanId=1224, StudentId=1954,StudentDegreeId=1850,PlanNumber=1, PlanAbbrev="Initial plan",PlanName="My initial plan "},
                    new StudentDegreePlan { StudentDegreePlanId = 1503, StudentId = 1531, StudentDegreeId = 1526, PlanNumber = 2, PlanAbbrev = "internship plan", PlanName = "Plan adjusted for internship" },
                    new StudentDegreePlan { StudentDegreePlanId = 1379, StudentId = 1647, StudentDegreeId = 1050, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1653, StudentId = 1840, StudentDegreeId = 1547, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1054, StudentId = 1537, StudentDegreeId = 1824, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1333, StudentId = 1675, StudentDegreeId = 1829, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1040, StudentId = 1745, StudentDegreeId = 1850, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1227, StudentId = 1206, StudentDegreeId = 1526, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1746, StudentId = 1966, StudentDegreeId = 1050, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1254, StudentId = 1683, StudentDegreeId = 1547, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1082, StudentId = 1333, StudentDegreeId = 1824, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1341, StudentId = 1333, StudentDegreeId = 1829, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1614, StudentId = 1606, StudentDegreeId = 1829, PlanNumber = 2, PlanAbbrev = "Most recent plan", PlanName = "My most recently updated plan (use this)" },
                    new StudentDegreePlan { StudentDegreePlanId = 1139, StudentId = 1954, StudentDegreeId = 1850, PlanNumber = 2, PlanAbbrev = "early comps", PlanName = "early comps" }
                        };
                foreach (StudentDegreePlan item in studentDegreePlans)
                {

                    try
                    {
                        context.StudentDegreePlans.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }
                }
            }

            if (!context.StudentDegreePlanTerms.Any())
            {

                var studentDegreePlanTerms = new StudentDegreePlanTerm[] {
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7712, StudentDegreePlanId = 1661, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8039, StudentDegreePlanId = 1661, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4734, StudentDegreePlanId = 1661, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6087, StudentDegreePlanId = 1661, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4535, StudentDegreePlanId = 1277, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1781, StudentDegreePlanId = 1277, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9815, StudentDegreePlanId = 1277, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9774, StudentDegreePlanId = 1277, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9362, StudentDegreePlanId = 1277, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4466, StudentDegreePlanId = 1277, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1453, StudentDegreePlanId = 1277, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6105, StudentDegreePlanId = 1277, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9309, StudentDegreePlanId = 1277, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7978, StudentDegreePlanId = 1082, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1326, StudentDegreePlanId = 1082, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4660, StudentDegreePlanId = 1082, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5745, StudentDegreePlanId = 1082, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2861, StudentDegreePlanId = 1082, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6307, StudentDegreePlanId = 1065, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6549, StudentDegreePlanId = 1065, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5389, StudentDegreePlanId = 1065, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1298, StudentDegreePlanId = 1065, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7140, StudentDegreePlanId = 1341, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7316, StudentDegreePlanId = 1341, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6388, StudentDegreePlanId = 1341, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3589, StudentDegreePlanId = 1341, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5641, StudentDegreePlanId = 1341, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6372, StudentDegreePlanId = 1672, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1873, StudentDegreePlanId = 1672, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7067, StudentDegreePlanId = 1672, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3624, StudentDegreePlanId = 1672, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6908, StudentDegreePlanId = 1503, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4703, StudentDegreePlanId = 1503, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9519, StudentDegreePlanId = 1503, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1429, StudentDegreePlanId = 1503, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7838, StudentDegreePlanId = 1503, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1702, StudentDegreePlanId = 1334, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5916, StudentDegreePlanId = 1334, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8121, StudentDegreePlanId = 1334, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1677, StudentDegreePlanId = 1334, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3807, StudentDegreePlanId = 1054, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2089, StudentDegreePlanId = 1054, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7154, StudentDegreePlanId = 1054, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5034, StudentDegreePlanId = 1054, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2505, StudentDegreePlanId = 1054, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1086, StudentDegreePlanId = 1105, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9970, StudentDegreePlanId = 1105, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3563, StudentDegreePlanId = 1105, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8923, StudentDegreePlanId = 1105, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3620, StudentDegreePlanId = 1614, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5820, StudentDegreePlanId = 1614, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4957, StudentDegreePlanId = 1614, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7837, StudentDegreePlanId = 1614, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5333, StudentDegreePlanId = 1614, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9356, StudentDegreePlanId = 1617, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2488, StudentDegreePlanId = 1617, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5167, StudentDegreePlanId = 1617, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6847, StudentDegreePlanId = 1617, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1179, StudentDegreePlanId = 1379, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8934, StudentDegreePlanId = 1379, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2448, StudentDegreePlanId = 1379, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4136, StudentDegreePlanId = 1379, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3630, StudentDegreePlanId = 1379, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9449, StudentDegreePlanId = 1234, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1146, StudentDegreePlanId = 1234, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5763, StudentDegreePlanId = 1234, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3687, StudentDegreePlanId = 1234, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3583, StudentDegreePlanId = 1333, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2040, StudentDegreePlanId = 1333, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8967, StudentDegreePlanId = 1333, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9153, StudentDegreePlanId = 1333, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9758, StudentDegreePlanId = 1333, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8251, StudentDegreePlanId = 1519, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3115, StudentDegreePlanId = 1519, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6570, StudentDegreePlanId = 1519, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1821, StudentDegreePlanId = 1519, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7998, StudentDegreePlanId = 1254, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1588, StudentDegreePlanId = 1254, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4074, StudentDegreePlanId = 1254, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4545, StudentDegreePlanId = 1254, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2576, StudentDegreePlanId = 1254, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1965, StudentDegreePlanId = 1076, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 4722, StudentDegreePlanId = 1076, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5963, StudentDegreePlanId = 1076, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3347, StudentDegreePlanId = 1076, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1693, StudentDegreePlanId = 1040, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5366, StudentDegreePlanId = 1040, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2520, StudentDegreePlanId = 1040, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2655, StudentDegreePlanId = 1040, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9306, StudentDegreePlanId = 1040, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7789, StudentDegreePlanId = 1147, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8271, StudentDegreePlanId = 1147, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6706, StudentDegreePlanId = 1147, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5024, StudentDegreePlanId = 1147, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3781, StudentDegreePlanId = 1653, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3780, StudentDegreePlanId = 1653, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1467, StudentDegreePlanId = 1653, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 7431, StudentDegreePlanId = 1653, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9674, StudentDegreePlanId = 1653, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5933, StudentDegreePlanId = 1224, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9423, StudentDegreePlanId = 1224, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1935, StudentDegreePlanId = 1224, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1317, StudentDegreePlanId = 1224, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1691, StudentDegreePlanId = 1139, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1173, StudentDegreePlanId = 1139, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1767, StudentDegreePlanId = 1139, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6234, StudentDegreePlanId = 1139, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2970, StudentDegreePlanId = 1139, TermNumber = 5, TermAbbrev = "Spring 2018" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1727, StudentDegreePlanId = 1133, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 6214, StudentDegreePlanId = 1133, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 3716, StudentDegreePlanId = 1133, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 2181, StudentDegreePlanId = 1133, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 5271, StudentDegreePlanId = 1746, TermNumber = 1, TermAbbrev = "Fall 2016" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 8145, StudentDegreePlanId = 1746, TermNumber = 2, TermAbbrev = "Spring 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1540, StudentDegreePlanId = 1746, TermNumber = 3, TermAbbrev = "Summer 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 9036, StudentDegreePlanId = 1746, TermNumber = 4, TermAbbrev = "Fall 2017" },
          new StudentDegreePlanTerm { StudentDegreePlanTermId = 1257, StudentDegreePlanId = 1746, TermNumber = 5, TermAbbrev = "Spring 2018" }
        };
                foreach (StudentDegreePlanTerm item in studentDegreePlanTerms)
                {

                    try
                    {
                        context.StudentDegreePlanTerms.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }
                }
            }

            if (!context.StudentDegreePlanTermRequirements.Any())
            {

                var studentDegreePlanTermRequirements = new StudentDegreePlanTermRequirement[] {
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30108, StudentDegreePlanTermId = 7712, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36602, StudentDegreePlanTermId = 7712, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48088, StudentDegreePlanTermId = 7712, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26972, StudentDegreePlanTermId = 7712, TermNumber = 1, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 20491, StudentDegreePlanTermId = 8039, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11253, StudentDegreePlanTermId = 8039, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34145, StudentDegreePlanTermId = 8039, TermNumber = 2, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38985, StudentDegreePlanTermId = 4734, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25452, StudentDegreePlanTermId = 4734, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47008, StudentDegreePlanTermId = 4734, TermNumber = 3, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11209, StudentDegreePlanTermId = 6087, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 17234, StudentDegreePlanTermId = 6087, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41208, StudentDegreePlanTermId = 6087, TermNumber = 4, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26072, StudentDegreePlanTermId = 4535, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30508, StudentDegreePlanTermId = 4535, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24092, StudentDegreePlanTermId = 4535, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13567, StudentDegreePlanTermId = 4535, TermNumber = 1, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 40548, StudentDegreePlanTermId = 1781, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25466, StudentDegreePlanTermId = 1781, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23894, StudentDegreePlanTermId = 1781, TermNumber = 2, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24103, StudentDegreePlanTermId = 9815, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26373, StudentDegreePlanTermId = 9774, TermNumber = 4, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 15133, StudentDegreePlanTermId = 9774, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30221, StudentDegreePlanTermId = 9774, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22882, StudentDegreePlanTermId = 9774, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34777, StudentDegreePlanTermId = 9362, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14044, StudentDegreePlanTermId = 9362, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39020, StudentDegreePlanTermId = 9362, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26173, StudentDegreePlanTermId = 9362, TermNumber = 5, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16903, StudentDegreePlanTermId = 6372, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34651, StudentDegreePlanTermId = 6372, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39336, StudentDegreePlanTermId = 6372, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 31825, StudentDegreePlanTermId = 6372, TermNumber = 1, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28169, StudentDegreePlanTermId = 1873, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46158, StudentDegreePlanTermId = 1873, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 40025, StudentDegreePlanTermId = 1873, TermNumber = 2, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21495, StudentDegreePlanTermId = 7067, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 44181, StudentDegreePlanTermId = 7067, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37419, StudentDegreePlanTermId = 7067, TermNumber = 3, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27761, StudentDegreePlanTermId = 3624, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16533, StudentDegreePlanTermId = 3624, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16275, StudentDegreePlanTermId = 3624, TermNumber = 4, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35124, StudentDegreePlanTermId = 6908, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14326, StudentDegreePlanTermId = 6908, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46874, StudentDegreePlanTermId = 6908, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34113, StudentDegreePlanTermId = 6908, TermNumber = 1, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25416, StudentDegreePlanTermId = 4703, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35556, StudentDegreePlanTermId = 4703, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36857, StudentDegreePlanTermId = 4703, TermNumber = 2, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25468, StudentDegreePlanTermId = 9519, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35967, StudentDegreePlanTermId = 1429, TermNumber = 4, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25331, StudentDegreePlanTermId = 1429, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37730, StudentDegreePlanTermId = 1429, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24728, StudentDegreePlanTermId = 1429, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29597, StudentDegreePlanTermId = 7838, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25300, StudentDegreePlanTermId = 7838, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 15283, StudentDegreePlanTermId = 7838, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16764, StudentDegreePlanTermId = 7838, TermNumber = 5, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43824, StudentDegreePlanTermId = 9356, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 42388, StudentDegreePlanTermId = 9356, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36696, StudentDegreePlanTermId = 9356, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 42816, StudentDegreePlanTermId = 2488, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 45351, StudentDegreePlanTermId = 2488, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27429, StudentDegreePlanTermId = 2488, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32019, StudentDegreePlanTermId = 5167, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28163, StudentDegreePlanTermId = 5167, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 44602, StudentDegreePlanTermId = 5167, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18258, StudentDegreePlanTermId = 6847, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13832, StudentDegreePlanTermId = 6847, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32977, StudentDegreePlanTermId = 6847, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 12710, StudentDegreePlanTermId = 6847, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36957, StudentDegreePlanTermId = 6847, TermNumber = 4, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37337, StudentDegreePlanTermId = 1179, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49145, StudentDegreePlanTermId = 1179, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35876, StudentDegreePlanTermId = 1179, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48232, StudentDegreePlanTermId = 8934, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39902, StudentDegreePlanTermId = 8934, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24816, StudentDegreePlanTermId = 8934, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 44649, StudentDegreePlanTermId = 2448, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26091, StudentDegreePlanTermId = 4136, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23420, StudentDegreePlanTermId = 4136, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28576, StudentDegreePlanTermId = 4136, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49458, StudentDegreePlanTermId = 3630, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 15584, StudentDegreePlanTermId = 3630, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22562, StudentDegreePlanTermId = 3630, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13136, StudentDegreePlanTermId = 3630, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39253, StudentDegreePlanTermId = 1727, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30186, StudentDegreePlanTermId = 1727, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28904, StudentDegreePlanTermId = 1727, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 40320, StudentDegreePlanTermId = 6214, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33155, StudentDegreePlanTermId = 6214, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41124, StudentDegreePlanTermId = 6214, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49441, StudentDegreePlanTermId = 3716, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35860, StudentDegreePlanTermId = 3716, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13031, StudentDegreePlanTermId = 3716, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35029, StudentDegreePlanTermId = 2181, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33622, StudentDegreePlanTermId = 2181, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18571, StudentDegreePlanTermId = 2181, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29982, StudentDegreePlanTermId = 2181, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48143, StudentDegreePlanTermId = 2181, TermNumber = 4, RequirementNumber = 14 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21909, StudentDegreePlanTermId = 5271, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22805, StudentDegreePlanTermId = 5271, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24764, StudentDegreePlanTermId = 5271, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22360, StudentDegreePlanTermId = 8145, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24163, StudentDegreePlanTermId = 8145, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43491, StudentDegreePlanTermId = 8145, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26824, StudentDegreePlanTermId = 1540, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11595, StudentDegreePlanTermId = 9036, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11393, StudentDegreePlanTermId = 9036, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29251, StudentDegreePlanTermId = 9036, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33206, StudentDegreePlanTermId = 1257, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39889, StudentDegreePlanTermId = 1257, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38840, StudentDegreePlanTermId = 1257, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23932, StudentDegreePlanTermId = 1257, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10738, StudentDegreePlanTermId = 7789, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22226, StudentDegreePlanTermId = 7789, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10730, StudentDegreePlanTermId = 7789, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34083, StudentDegreePlanTermId = 8271, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18773, StudentDegreePlanTermId = 8271, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13604, StudentDegreePlanTermId = 8271, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 12649, StudentDegreePlanTermId = 6706, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37761, StudentDegreePlanTermId = 6706, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35203, StudentDegreePlanTermId = 6706, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49014, StudentDegreePlanTermId = 5024, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29733, StudentDegreePlanTermId = 5024, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26011, StudentDegreePlanTermId = 5024, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 19903, StudentDegreePlanTermId = 5024, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30532, StudentDegreePlanTermId = 3781, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14206, StudentDegreePlanTermId = 3781, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23677, StudentDegreePlanTermId = 3781, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 19255, StudentDegreePlanTermId = 3780, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16975, StudentDegreePlanTermId = 3780, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 42209, StudentDegreePlanTermId = 3780, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 20932, StudentDegreePlanTermId = 1467, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13601, StudentDegreePlanTermId = 7431, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27674, StudentDegreePlanTermId = 7431, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21773, StudentDegreePlanTermId = 7431, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16875, StudentDegreePlanTermId = 9674, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28311, StudentDegreePlanTermId = 9674, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46031, StudentDegreePlanTermId = 9674, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39895, StudentDegreePlanTermId = 9674, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29686, StudentDegreePlanTermId = 8251, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11503, StudentDegreePlanTermId = 8251, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39408, StudentDegreePlanTermId = 8251, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14373, StudentDegreePlanTermId = 3115, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16884, StudentDegreePlanTermId = 3115, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33240, StudentDegreePlanTermId = 3115, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10733, StudentDegreePlanTermId = 6570, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30431, StudentDegreePlanTermId = 6570, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 15484, StudentDegreePlanTermId = 6570, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16905, StudentDegreePlanTermId = 1821, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48335, StudentDegreePlanTermId = 1821, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 44904, StudentDegreePlanTermId = 1821, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49616, StudentDegreePlanTermId = 1821, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41757, StudentDegreePlanTermId = 7998, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46209, StudentDegreePlanTermId = 7998, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35004, StudentDegreePlanTermId = 7998, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35025, StudentDegreePlanTermId = 1588, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10206, StudentDegreePlanTermId = 1588, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48851, StudentDegreePlanTermId = 1588, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30823, StudentDegreePlanTermId = 4074, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38492, StudentDegreePlanTermId = 4545, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28463, StudentDegreePlanTermId = 4545, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34700, StudentDegreePlanTermId = 4545, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29841, StudentDegreePlanTermId = 2576, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26889, StudentDegreePlanTermId = 2576, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37843, StudentDegreePlanTermId = 2576, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34345, StudentDegreePlanTermId = 2576, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34211, StudentDegreePlanTermId = 1702, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13450, StudentDegreePlanTermId = 1702, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32309, StudentDegreePlanTermId = 1702, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37677, StudentDegreePlanTermId = 5916, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14631, StudentDegreePlanTermId = 5916, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22789, StudentDegreePlanTermId = 5916, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 19973, StudentDegreePlanTermId = 8121, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47907, StudentDegreePlanTermId = 8121, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23753, StudentDegreePlanTermId = 8121, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22939, StudentDegreePlanTermId = 1677, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33715, StudentDegreePlanTermId = 1677, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 31644, StudentDegreePlanTermId = 1677, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23156, StudentDegreePlanTermId = 3807, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26105, StudentDegreePlanTermId = 3807, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47102, StudentDegreePlanTermId = 3807, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37525, StudentDegreePlanTermId = 2089, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46634, StudentDegreePlanTermId = 2089, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46753, StudentDegreePlanTermId = 2089, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29363, StudentDegreePlanTermId = 7154, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43367, StudentDegreePlanTermId = 5034, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39555, StudentDegreePlanTermId = 5034, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35781, StudentDegreePlanTermId = 5034, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23008, StudentDegreePlanTermId = 2505, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41896, StudentDegreePlanTermId = 2505, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13767, StudentDegreePlanTermId = 2505, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 17051, StudentDegreePlanTermId = 4466, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38292, StudentDegreePlanTermId = 4466, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29831, StudentDegreePlanTermId = 4466, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41014, StudentDegreePlanTermId = 1453, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37791, StudentDegreePlanTermId = 1453, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23275, StudentDegreePlanTermId = 1453, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 12504, StudentDegreePlanTermId = 6105, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49031, StudentDegreePlanTermId = 6105, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32117, StudentDegreePlanTermId = 6105, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11571, StudentDegreePlanTermId = 9309, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47158, StudentDegreePlanTermId = 9309, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25118, StudentDegreePlanTermId = 9309, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 14037, StudentDegreePlanTermId = 7978, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30302, StudentDegreePlanTermId = 7978, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34142, StudentDegreePlanTermId = 7978, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23033, StudentDegreePlanTermId = 1326, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29854, StudentDegreePlanTermId = 1326, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38713, StudentDegreePlanTermId = 1326, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10771, StudentDegreePlanTermId = 4660, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24850, StudentDegreePlanTermId = 5745, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16579, StudentDegreePlanTermId = 5745, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37432, StudentDegreePlanTermId = 5745, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21763, StudentDegreePlanTermId = 2861, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49427, StudentDegreePlanTermId = 2861, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 12952, StudentDegreePlanTermId = 2861, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35568, StudentDegreePlanTermId = 6307, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35819, StudentDegreePlanTermId = 6307, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34516, StudentDegreePlanTermId = 6307, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32207, StudentDegreePlanTermId = 6549, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46428, StudentDegreePlanTermId = 6549, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48850, StudentDegreePlanTermId = 6549, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38951, StudentDegreePlanTermId = 5389, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 45600, StudentDegreePlanTermId = 5389, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32246, StudentDegreePlanTermId = 5389, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29763, StudentDegreePlanTermId = 1298, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 16148, StudentDegreePlanTermId = 1298, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29769, StudentDegreePlanTermId = 1298, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29713, StudentDegreePlanTermId = 1298, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10649, StudentDegreePlanTermId = 7140, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47032, StudentDegreePlanTermId = 7140, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36336, StudentDegreePlanTermId = 7140, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23959, StudentDegreePlanTermId = 7316, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23098, StudentDegreePlanTermId = 7316, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26971, StudentDegreePlanTermId = 7316, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 45139, StudentDegreePlanTermId = 6388, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24873, StudentDegreePlanTermId = 3589, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37232, StudentDegreePlanTermId = 3589, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32209, StudentDegreePlanTermId = 3589, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47948, StudentDegreePlanTermId = 5641, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35857, StudentDegreePlanTermId = 5641, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34890, StudentDegreePlanTermId = 5641, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24133, StudentDegreePlanTermId = 5641, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21013, StudentDegreePlanTermId = 1086, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 49871, StudentDegreePlanTermId = 1086, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 45827, StudentDegreePlanTermId = 1086, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 15706, StudentDegreePlanTermId = 9970, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18542, StudentDegreePlanTermId = 9970, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39725, StudentDegreePlanTermId = 9970, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 44913, StudentDegreePlanTermId = 3563, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33782, StudentDegreePlanTermId = 3563, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39440, StudentDegreePlanTermId = 3563, TermNumber = 3, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43927, StudentDegreePlanTermId = 8923, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24057, StudentDegreePlanTermId = 8923, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 28083, StudentDegreePlanTermId = 8923, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32210, StudentDegreePlanTermId = 8923, TermNumber = 4, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 47154, StudentDegreePlanTermId = 3620, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 46023, StudentDegreePlanTermId = 3620, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 34812, StudentDegreePlanTermId = 3620, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23131, StudentDegreePlanTermId = 5820, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21729, StudentDegreePlanTermId = 5820, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25200, StudentDegreePlanTermId = 5820, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30723, StudentDegreePlanTermId = 4957, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23157, StudentDegreePlanTermId = 7837, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43429, StudentDegreePlanTermId = 7837, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11514, StudentDegreePlanTermId = 7837, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27925, StudentDegreePlanTermId = 5333, TermNumber = 5, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 26615, StudentDegreePlanTermId = 5333, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 19789, StudentDegreePlanTermId = 5333, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25083, StudentDegreePlanTermId = 5333, TermNumber = 5, RequirementNumber = 13 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13723, StudentDegreePlanTermId = 5933, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 22780, StudentDegreePlanTermId = 5933, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 42805, StudentDegreePlanTermId = 5933, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 25997, StudentDegreePlanTermId = 9423, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30937, StudentDegreePlanTermId = 9423, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 29201, StudentDegreePlanTermId = 9423, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30251, StudentDegreePlanTermId = 1935, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35113, StudentDegreePlanTermId = 1935, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 31404, StudentDegreePlanTermId = 1935, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38092, StudentDegreePlanTermId = 1317, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36769, StudentDegreePlanTermId = 1317, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37511, StudentDegreePlanTermId = 1317, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35199, StudentDegreePlanTermId = 1691, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 10538, StudentDegreePlanTermId = 1691, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32453, StudentDegreePlanTermId = 1691, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 45538, StudentDegreePlanTermId = 1173, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18839, StudentDegreePlanTermId = 1173, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 31016, StudentDegreePlanTermId = 1173, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35931, StudentDegreePlanTermId = 1767, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18856, StudentDegreePlanTermId = 6234, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43217, StudentDegreePlanTermId = 6234, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27285, StudentDegreePlanTermId = 6234, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 36079, StudentDegreePlanTermId = 2970, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 21073, StudentDegreePlanTermId = 2970, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 39978, StudentDegreePlanTermId = 2970, TermNumber = 5, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41097, StudentDegreePlanTermId = 1965, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 11081, StudentDegreePlanTermId = 1965, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 13541, StudentDegreePlanTermId = 1965, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43003, StudentDegreePlanTermId = 4722, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24962, StudentDegreePlanTermId = 4722, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 37061, StudentDegreePlanTermId = 4722, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 27656, StudentDegreePlanTermId = 5963, TermNumber = 3, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23353, StudentDegreePlanTermId = 5963, TermNumber = 3, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 33738, StudentDegreePlanTermId = 5963, TermNumber = 3, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 24694, StudentDegreePlanTermId = 3347, TermNumber = 4, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 31321, StudentDegreePlanTermId = 3347, TermNumber = 4, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 40408, StudentDegreePlanTermId = 3347, TermNumber = 4, RequirementNumber = 12 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 38555, StudentDegreePlanTermId = 1693, TermNumber = 1, RequirementNumber = 1 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 35493, StudentDegreePlanTermId = 1693, TermNumber = 1, RequirementNumber = 2 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43974, StudentDegreePlanTermId = 1693, TermNumber = 1, RequirementNumber = 3 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 18373, StudentDegreePlanTermId = 5366, TermNumber = 2, RequirementNumber = 4 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23088, StudentDegreePlanTermId = 5366, TermNumber = 2, RequirementNumber = 5 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 17483, StudentDegreePlanTermId = 5366, TermNumber = 2, RequirementNumber = 6 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 23451, StudentDegreePlanTermId = 2520, TermNumber = 3, RequirementNumber = 0 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 32316, StudentDegreePlanTermId = 2655, TermNumber = 4, RequirementNumber = 7 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 20423, StudentDegreePlanTermId = 2655, TermNumber = 4, RequirementNumber = 8 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 48443, StudentDegreePlanTermId = 2655, TermNumber = 4, RequirementNumber = 9 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 43663, StudentDegreePlanTermId = 9306, TermNumber = 5, RequirementNumber = 10 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 41411, StudentDegreePlanTermId = 9306, TermNumber = 5, RequirementNumber = 11 },
          new StudentDegreePlanTermRequirement { StudentDegreePlanTermRequirementId = 30109, StudentDegreePlanTermId = 9306, TermNumber = 5, RequirementNumber = 12 }
        };
                foreach (StudentDegreePlanTermRequirement item in studentDegreePlanTermRequirements)
                {

                    try
                    {
                        context.StudentDegreePlanTermRequirements.Add(item);
                        context.SaveChanges();
                        LOG.LogDebug("Added {}", item);
                    }
                    catch (Exception ex)
                    {
                        LOG.LogError("Error adding {}. Inner Exception was {}.", item, ex.InnerException.Message);
                    }
                }
            }
        }
    }
}
