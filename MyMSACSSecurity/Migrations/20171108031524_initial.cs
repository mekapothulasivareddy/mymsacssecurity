﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace MyMSACSSecurity.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Name = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AccessFailedCount = table.Column<int>(type: "int", nullable: false),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnabled = table.Column<bool>(type: "bit", nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    NormalizedEmail = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true),
                    PasswordHash = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(type: "bit", nullable: false),
                    SecurityStamp = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    TwoFactorEnabled = table.Column<bool>(type: "bit", nullable: false),
                    UserName = table.Column<string>(type: "nvarchar(256)", maxLength: 256, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentId = table.Column<int>(type: "int", nullable: false),
                    FamilyName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    GivenName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentId);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegree",
                columns: table => new
                {
                    StudentDegreeId = table.Column<int>(type: "int", nullable: false),
                    DegreeAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    DegreeName = table.Column<string>(type: "nvarchar(110)", maxLength: 110, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegree", x => x.StudentDegreeId);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreeRequirementStatus",
                columns: table => new
                {
                    StudentDegreeRequirementStatusId = table.Column<int>(type: "int", nullable: false),
                    RequirementStatus = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreeRequirementStatus", x => x.StudentDegreeRequirementStatusId);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreeStatus",
                columns: table => new
                {
                    StudentDegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    DegreeStatus = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreeStatus", x => x.StudentDegreeStatusId);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    LoginProvider = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreeRequirement",
                columns: table => new
                {
                    StudentDegreeRequirementId = table.Column<int>(type: "int", nullable: false),
                    RequirementAbbrev = table.Column<string>(type: "nvarchar(10)", maxLength: 10, nullable: false),
                    RequirementName = table.Column<string>(type: "nvarchar(60)", maxLength: 60, nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false),
                    StudentDegreeId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreeRequirement", x => x.StudentDegreeRequirementId);
                    table.ForeignKey(
                        name: "FK_StudentDegreeRequirement_StudentDegree_StudentDegreeId",
                        column: x => x.StudentDegreeId,
                        principalTable: "StudentDegree",
                        principalColumn: "StudentDegreeId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreePlan",
                columns: table => new
                {
                    StudentDegreePlanId = table.Column<int>(type: "int", nullable: false),
                    IncludesInternship = table.Column<bool>(type: "bit", nullable: false),
                    PlanAbbrev = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: false),
                    PlanName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    PlanNumber = table.Column<int>(type: "int", maxLength: 10, nullable: false),
                    StudentDegreeId = table.Column<int>(type: "int", nullable: false),
                    StudentDegreeStatusId = table.Column<int>(type: "int", nullable: false),
                    StudentId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreePlan", x => x.StudentDegreePlanId);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_StudentDegree_StudentDegreeId",
                        column: x => x.StudentDegreeId,
                        principalTable: "StudentDegree",
                        principalColumn: "StudentDegreeId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_StudentDegreeStatus_StudentDegreeStatusId",
                        column: x => x.StudentDegreeStatusId,
                        principalTable: "StudentDegreeStatus",
                        principalColumn: "StudentDegreeStatusId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlan_Student_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Student",
                        principalColumn: "StudentId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreePlanTerm",
                columns: table => new
                {
                    StudentDegreePlanTermId = table.Column<int>(type: "int", nullable: false),
                    StudentDegreePlanId = table.Column<int>(type: "int", nullable: false),
                    TermAbbrev = table.Column<string>(type: "nvarchar(20)", maxLength: 20, nullable: true),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreePlanTerm", x => x.StudentDegreePlanTermId);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlanTerm_StudentDegreePlan_StudentDegreePlanId",
                        column: x => x.StudentDegreePlanId,
                        principalTable: "StudentDegreePlan",
                        principalColumn: "StudentDegreePlanId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentDegreePlanTermRequirement",
                columns: table => new
                {
                    StudentDegreePlanTermRequirementId = table.Column<int>(type: "int", nullable: false),
                    RequirementNumber = table.Column<int>(type: "int", nullable: false),
                    StudentDegreePlanTermId = table.Column<int>(type: "int", nullable: false),
                    StudentDegreeRequirementStatusId = table.Column<int>(type: "int", nullable: false),
                    TermNumber = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentDegreePlanTermRequirement", x => x.StudentDegreePlanTermRequirementId);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlanTermRequirement_StudentDegreePlanTerm_StudentDegreePlanTermId",
                        column: x => x.StudentDegreePlanTermId,
                        principalTable: "StudentDegreePlanTerm",
                        principalColumn: "StudentDegreePlanTermId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentDegreePlanTermRequirement_StudentDegreeRequirementStatus_StudentDegreeRequirementStatusId",
                        column: x => x.StudentDegreeRequirementStatusId,
                        principalTable: "StudentDegreeRequirementStatus",
                        principalColumn: "StudentDegreeRequirementStatusId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_StudentDegreeId",
                table: "StudentDegreePlan",
                column: "StudentDegreeId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_StudentDegreeStatusId",
                table: "StudentDegreePlan",
                column: "StudentDegreeStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlan_StudentId",
                table: "StudentDegreePlan",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlanTerm_StudentDegreePlanId",
                table: "StudentDegreePlanTerm",
                column: "StudentDegreePlanId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlanTermRequirement_StudentDegreePlanTermId",
                table: "StudentDegreePlanTermRequirement",
                column: "StudentDegreePlanTermId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreePlanTermRequirement_StudentDegreeRequirementStatusId",
                table: "StudentDegreePlanTermRequirement",
                column: "StudentDegreeRequirementStatusId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentDegreeRequirement_StudentDegreeId",
                table: "StudentDegreeRequirement",
                column: "StudentDegreeId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "StudentDegreePlanTermRequirement");

            migrationBuilder.DropTable(
                name: "StudentDegreeRequirement");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "StudentDegreePlanTerm");

            migrationBuilder.DropTable(
                name: "StudentDegreeRequirementStatus");

            migrationBuilder.DropTable(
                name: "StudentDegreePlan");

            migrationBuilder.DropTable(
                name: "StudentDegree");

            migrationBuilder.DropTable(
                name: "StudentDegreeStatus");

            migrationBuilder.DropTable(
                name: "Student");
        }
    }
}
