﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACSSecurity.Data;
using MyMSACSSecurity.Models;

namespace MyMSACSSecurity.Controllers
{
    public class StudentDegreePlansController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentDegreePlansController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: StudentDegreePlans
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.StudentDegreePlans.Include(s => s.Student).Include(s => s.StudentDegree).Include(s => s.StudentDegreeStatus);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: StudentDegreePlans/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans
                .Include(s => s.Student)
                .Include(s => s.StudentDegree)
                .Include(s => s.StudentDegreeStatus)
                .SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }

            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Create
        public IActionResult Create()
        {
            ViewData["StudentId"] = new SelectList(_context.Students, "StudentId", "FamilyName");
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev");
            ViewData["StudentDegreeStatusId"] = new SelectList(_context.StudentDegreeStatuses, "StudentDegreeStatusId", "DegreeStatus");
            return View();
        }

        // POST: StudentDegreePlans/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentDegreePlanId,StudentId,StudentDegreeId,PlanNumber,PlanAbbrev,PlanName,StudentDegreeStatusId,IncludesInternship")] StudentDegreePlan studentDegreePlan)
        {
            if (ModelState.IsValid)
            {
                _context.Add(studentDegreePlan);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StudentId"] = new SelectList(_context.Students, "StudentId", "FamilyName", studentDegreePlan.StudentId);
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", studentDegreePlan.StudentDegreeId);
            ViewData["StudentDegreeStatusId"] = new SelectList(_context.StudentDegreeStatuses, "StudentDegreeStatusId", "DegreeStatus", studentDegreePlan.StudentDegreeStatusId);
            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans.SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }
            ViewData["StudentId"] = new SelectList(_context.Students, "StudentId", "FamilyName", studentDegreePlan.StudentId);
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", studentDegreePlan.StudentDegreeId);
            ViewData["StudentDegreeStatusId"] = new SelectList(_context.StudentDegreeStatuses, "StudentDegreeStatusId", "DegreeStatus", studentDegreePlan.StudentDegreeStatusId);
            return View(studentDegreePlan);
        }

        // POST: StudentDegreePlans/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentDegreePlanId,StudentId,StudentDegreeId,PlanNumber,PlanAbbrev,PlanName,StudentDegreeStatusId,IncludesInternship")] StudentDegreePlan studentDegreePlan)
        {
            if (id != studentDegreePlan.StudentDegreePlanId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(studentDegreePlan);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StudentDegreePlanExists(studentDegreePlan.StudentDegreePlanId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StudentId"] = new SelectList(_context.Students, "StudentId", "FamilyName", studentDegreePlan.StudentId);
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", studentDegreePlan.StudentDegreeId);
            ViewData["StudentDegreeStatusId"] = new SelectList(_context.StudentDegreeStatuses, "StudentDegreeStatusId", "DegreeStatus", studentDegreePlan.StudentDegreeStatusId);
            return View(studentDegreePlan);
        }

        // GET: StudentDegreePlans/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var studentDegreePlan = await _context.StudentDegreePlans
                .Include(s => s.Student)
                .Include(s => s.StudentDegree)
                .Include(s => s.StudentDegreeStatus)
                .SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            if (studentDegreePlan == null)
            {
                return NotFound();
            }

            return View(studentDegreePlan);
        }

        // POST: StudentDegreePlans/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var studentDegreePlan = await _context.StudentDegreePlans.SingleOrDefaultAsync(m => m.StudentDegreePlanId == id);
            _context.StudentDegreePlans.Remove(studentDegreePlan);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool StudentDegreePlanExists(int id)
        {
            return _context.StudentDegreePlans.Any(e => e.StudentDegreePlanId == id);
        }
    }
}
