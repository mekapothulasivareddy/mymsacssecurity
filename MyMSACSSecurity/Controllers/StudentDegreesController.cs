﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MyMSACSSecurity.Data;
using MyMSACSSecurity.Models;

namespace MyMSACSSecurity.Controllers
{
    public class StudentDegreesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentDegreesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Degrees
        public async Task<IActionResult> Index(string sortOrder, string searchString)
        {
            ViewData["AbbrevSortParam"] = String.IsNullOrEmpty(sortOrder) ? "abbrev_desc" : "";
            ViewData["NameSortParam"] = sortOrder == "name" ? "name_desc" : "name";
            ViewData["CurrentFilter"] = searchString;

            var degrees = from d in _context.StudentDegrees
                          select d;

            if (!String.IsNullOrEmpty(searchString))
            {
                degrees = degrees.Where(d => d.DegreeAbbrev.Contains(searchString) ||
                 d.DegreeName.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "abbrev_desc":
                    degrees = degrees.OrderByDescending(d => d.DegreeAbbrev);
                    break;
                case "name_desc":
                    degrees = degrees.OrderByDescending(d => d.DegreeName);
                    break;
                case "name":
                    degrees = degrees.OrderBy(d => d.DegreeName);
                    break;
                default:
                    degrees = degrees.OrderBy(d => d.DegreeAbbrev);
                    break;
            }
            return View(await degrees.AsNoTracking().ToListAsync());
        }

        // GET: Degrees/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degree = await _context.StudentDegrees
              .Include(d => d.StudentDegreeRequirements)
              .SingleOrDefaultAsync(m => m.StudentDegreeId == id);

            if (degree == null)
            {
                return NotFound();
            }

            return View(degree);
        }

        // GET: Degrees/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Degrees/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentDegreeId,DegreeAbbrev,DegreeName")] StudentDegree degree)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _context.Add(degree);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            catch (DbUpdateException ex)
            {
                ModelState.AddModelError("", "Unable to save changes. " + ex.Message + " " + ex.InnerException);
            }
            return View(degree);
        }

        // GET: Degrees/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeToUpdate = await _context.StudentDegrees.SingleOrDefaultAsync(m => m.StudentDegreeId == id);

            if (degreeToUpdate == null)
            {
                return NotFound();
            }
            if (await TryUpdateModelAsync<StudentDegree>(
                degreeToUpdate,
                "",
                m => m.DegreeAbbrev, m => m.DegreeName))
            {
                try
                {
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (DbUpdateException ex)
                {
                    ModelState.AddModelError("", "Unable to save changes. " + ex.Message + " " + ex.InnerException);
                }
            }
            return View(degreeToUpdate);
        }

        // POST: Degrees/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentDegreeId,DegreeAbbrev,DegreeName")] StudentDegree degree)
        {
            if (id != degree.StudentDegreeId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(degree);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DegreeExists(degree.StudentDegreeId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(degree);
        }

        // GET: Degrees/Delete/5
        public async Task<IActionResult> Delete(int? id, bool? saveChangesError = false)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degree = await _context.StudentDegrees
              .AsNoTracking()
              .SingleOrDefaultAsync(m => m.StudentDegreeId == id);
            if (degree == null)
            {
                return NotFound();
            }
            if (saveChangesError.GetValueOrDefault())
            {
                ViewData["ErrorMessage"] =
                  "Delete failed. Try again.";
            }

            return View(degree);
        }

        // POST: Degrees/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var degree = await _context.StudentDegrees.SingleOrDefaultAsync(m => m.StudentDegreeId == id);

            if (degree == null)
            {
                return RedirectToAction(nameof(Index));
            }

            try
            {
                _context.StudentDegrees.Remove(degree);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (DbUpdateException)
            {
                return RedirectToAction(nameof(Delete), new { id = id, saveChangesError = true });
            }
        }

        private bool DegreeExists(int id)
        {
            return _context.StudentDegrees.Any(e => e.StudentDegreeId == id);
        }
    }
}