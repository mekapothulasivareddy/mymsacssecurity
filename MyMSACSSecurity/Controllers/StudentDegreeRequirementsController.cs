﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MyMSACSSecurity.Data;
using MyMSACSSecurity.Models;

namespace MyMSACSSecurity.Controllers
{
    public class StudentDegreeRequirementsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public StudentDegreeRequirementsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: DegreeRequirements
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.StudentDegreeRequirements.Include(d => d.StudentDegree);
            return View(await applicationDbContext.ToListAsync());
        }

        // GET: DegreeRequirements/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeRequirement = await _context.StudentDegreeRequirements
              .Include(d => d.StudentDegree)
              .SingleOrDefaultAsync(m => m.StudentDegreeRequirementId == id);
            if (degreeRequirement == null)
            {
                return NotFound();
            }

            return View(degreeRequirement);
        }

        // GET: DegreeRequirements/Create
        public IActionResult Create()
        {
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev");
            return View();
        }

        // POST: DegreeRequirements/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StudentDegreeRequirementId,StudentDegreeId,RequirementNumber,RequirementAbbrev,RequirementName")] StudentDegreeRequirement degreeRequirement)
        {
            if (ModelState.IsValid)
            {
                _context.Add(degreeRequirement);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", degreeRequirement.StudentDegreeId);
            return View(degreeRequirement);
        }

        // GET: DegreeRequirements/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeRequirement = await _context.StudentDegreeRequirements.SingleOrDefaultAsync(m => m.StudentDegreeRequirementId == id);
            if (degreeRequirement == null)
            {
                return NotFound();
            }
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", degreeRequirement.StudentDegreeId);
            return View(degreeRequirement);
        }

        // POST: DegreeRequirements/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StudentDegreeRequirementId,StudentDegreeId,RequirementNumber,RequirementAbbrev,RequirementName")] StudentDegreeRequirement degreeRequirement)
        {
            if (id != degreeRequirement.StudentDegreeRequirementId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(degreeRequirement);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DegreeRequirementExists(degreeRequirement.StudentDegreeRequirementId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["StudentDegreeId"] = new SelectList(_context.StudentDegrees, "StudentDegreeId", "DegreeAbbrev", degreeRequirement.StudentDegreeId);
            return View(degreeRequirement);
        }

        // GET: DegreeRequirements/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var degreeRequirement = await _context.StudentDegreeRequirements
              .Include(d => d.StudentDegree)
              .SingleOrDefaultAsync(m => m.StudentDegreeRequirementId == id);
            if (degreeRequirement == null)
            {
                return NotFound();
            }

            return View(degreeRequirement);
        }

        // POST: DegreeRequirements/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var degreeRequirement = await _context.StudentDegreeRequirements.SingleOrDefaultAsync(m => m.StudentDegreeRequirementId == id);
            _context.StudentDegreeRequirements.Remove(degreeRequirement);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DegreeRequirementExists(int id)
        {
            return _context.StudentDegreeRequirements.Any(e => e.StudentDegreeRequirementId == id);
        }
    }
}